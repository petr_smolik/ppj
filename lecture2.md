# 1. Describe JVM heap and stack. Which variables are stored on heap and which on stack
- Zásobník je část paměti vyhrazená pro procedury (funkce). Jsou tam uložené lokální proměnné procedury a její argumenty. Každá procedura má vlastní blok paměti, který vzniká při zavolání procedury a zaniká, když je procedura dokončena.
- Halda je část paměti, která slouží k ukládání definicí tříd a instancí objektů a polí.

# 2. How does GC work
- Garbage collector zodpovídá za automatickou správu paměti. Jeho hlavním úkolem je mazat objekty z haldy, které už nebudou použity (již na ně neexistuje reference). Je volán, když je nedostatek volné paměti.
## 1. Stop the world
- GB pracuje v cyklech. Aby mohl GB mohl dělat svoji práci, musí při každém cyklu zastavit všechna vlákna. Proto všechny vládkna mají tzv. safe pointy - místa, kde mohou být bezpečně zastavena.
## 2. GC roots
- Jsou to speciállní objekty, které GB používá jako vstupní body do grafu aktivních objektů. Grafů aktivních objektů je více - např. třídy, vlákna nebo lokální proměnné v zásobníku. GB projde všechny grafy a označí všechny aktivní objekty. Neoznačené objekty smaže. GB roots nejsou nikdy smazány.

# 3. Describe G1 collector
- G1C je navržený pro multiprocesorové počítače s velkou pamětí. Čistí pamět za běhu aplikace (má hodně krátké pauzy pro čistění, sdílí zdroje s aplikací) . Rozděluje haldu na stejně velké bloky. Funguje ve 2 fázích, v první fázi projde všechny bloky a zjistí v kolik v nich je aktivních objektů. Ve 2. fázy vybírá ty bloky, kde se vyskytuje nejméně aktivních objektů, dokud nezíská potřebné volné paměťové místo.

# 4. Describe ZGC collector
- ZGC běží podobně jako G1C souběžně s aplikací, ale jestě s nižší latencí. Zaručuje, že nezastaví vlákno aplikace na déle než 10 ms. V referencích objektů si ukládá extra informaci o stavu objektu (zda se ještě používá). Tyto reference se označují jako colored pointers. Halda je opět rozdělena do bloků, které ale mohou mít rozdílné velikosti.

# 5. Compare G1 vs ZGC
- ZGC se více hodí pro aplikace, které vyžadují nízkou latenci. Jeho efektivita se nesnižuje se zvyšující se velikosti haldy, funguje tak dobře i na velmi velké haldě. Lze použít pouze na 64-bitových platformách (kvůli colored references).
- G1 má o něco vyšší latenci (v řádu nízkých stovek ms), hodí se pro paměti s vysokou fragmentací a pro velké haldy, které jsou aspoň z poloviny zaplněné.

# 6. Describe bytecode (groups, prefix/suffix, operand types)
- Bytecode je instrukční sada JVM. Je generován při kompilaci java programu. Bytecode je uložen v souboru s příponou .class. Bytecode umožňuje javě být platformě nezávislá.

![pic2.png](./pic2.png)

![pic1.png](./pic1.png)

# 7. How is bytecode generated and how can be viewed
- Je generován při kompilaci java programu (přípona .java). Bytecode je generován pro každou metodu v programu zvlášť do souborou .class.
- Bytecode může být zobrazen např. nástrojem příkazové řádky javap, frameworkem ASM, pluginem pro IntelliJ IDEA jclasslib, nebo knihovnami Javassist a BCEL.

# 8. Describe operand stack and local variables array
- Pro každou metodu je v zásovníku vyhrazený frame. Tento frame obsahuje zásobník operandů a pole lokálních proměnných. Velikosti těchto objektů jsou určeny při kompilaci.
- Proměnné v poli lokální proměnných mohou být datového typu boolean, byte, char, short, int, float, reference, returnAddress, long, nebo double. Poslední dvě zabírají dvě lokální proměnné v poli. Pole je indexováno čísli, začíná v 0. JVM používá toto pole pro předáváná parametrů volané metodě (od indexu 1). Na indexu 0 je vždycky reference na objekt, z kterého je metoda volána.
- Zásobník operandů je LIFO. Po vytvoření frame je zásobník prázdný. Pomocí instrukcí JVM je do něj možné přesouvat proměnné z pole lokálních proměnných. Slouží hlavně k provádění výpočtů a operací nad proměnnýma, kreré do něj byly uloženy (př. iadd nad proměnnýma na vrcholu zásobníku). Možné jsou i mezivýpočty. Také se používá pro odeslání paremetrů do nové volané metody a pro získání jejího výsledku. Pro typy proměnných platí to samé jako u pole lokálních proměnných.

# 9. Describe how does bytecode interpretation works in runtime
- Za iterpretaci bytekódu zodpovídá JVM. JVM je softwarový stroj, který vykonává instrukce bytekódu. Jeho součástí je classloader, který bytekód převádí na třídy (při prvním použití). Třídy jsou hledány na classpath. Interpretace začíná od specifikované třídy a její metodou main. Ze začátku jsou instrukce pouze interpretovány, poté jsou často volané instrukce překládány do nativního kódu procesoru (JIT kompilace). Bytekód je interpretován řádek po řádku.

# 10. What is JIT compilation, how does it work
- Při spuštění programu JVM část bytekódu překládá do nativních instrukcí daného procesoru. Překládají se ty nejčastěji volané části, to vede ke zrychlení překladu programu. Nejčastěji volané části jsou získány analýzovou kódu během překladu, o každé metodě je ukládána informace, kolikrát byla volána. Jsou 2 úrovně optimalizace JIT kompilace: klient (pro desktopové aplikace) a server. Překlad u klientské úrovně je rychlejší a produkuje méně optimalizovaný kód. U serveru je překlad podstatně delší, ale kód je více optimalizovaný.
